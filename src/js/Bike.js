//var TEMPLATE_ENGINE = 'ejs';
//var TEMPLATE_ENGINE = 'handlebars';
var TEMPLATE_ENGINE = 'jade';

define([
    'jquery',
    'template/menu.' + TEMPLATE_ENGINE,
    'template/images.' + TEMPLATE_ENGINE,
    'template/characteristics.' + TEMPLATE_ENGINE
], function ($, menuTemplate, imagesTemplate, characteristicsTemplate) {
    function Bike(bike, $parentElement) {
        this.bikeId = bike.id;
        this.$body = $parentElement;

        var expanded = false;
        $('<a href="#">')
            .text(bike.vendor + ' ' + bike.name)
            .appendTo($('<h3></h3>').appendTo(this.$body))
            .click(function (e) {
                e.preventDefault();
                this.load();
                this.$body.toggleClass('expanded', expanded = !expanded);
            }.bind(this));
    }

    Bike.prototype.load = function () {
        if (this.status == 'loading' || this.status == 'loaded') {
            return;
        }

        this.status = 'loading';

        var loading = $('<div>').addClass('bike-loading').appendTo(this.$body);

        $.get('./data/bike/' + this.bikeId + '.json', function (bike) {
            loading.remove();
            this.status = 'loaded';

            var $menu = $(menuTemplate({bike: bike})).appendTo(this.$body);
            var $data = $('<div>').addClass('bike-data').appendTo(this.$body);

            $menu.find('[data-tab]').click(function () {
                $menu.find('li').removeClass('active');
                $(this).addClass('active');
            });

            $menu.find('[data-tab=images]').click(function () {
                $data.empty().append(imagesTemplate({bike: bike}));
            });

            $menu.find('[data-tab=characteristics]').click(function () {
                $data.empty().append(characteristicsTemplate({bike: bike}));
            });

            $menu.find('li:first').click();
        }.bind(this));
    };

    return Bike;
});

